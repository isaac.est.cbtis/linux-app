# Linux App

App en tres capas en Linux


Seguimos con la arquitectura a tres capas (3-Tier Architecture) y damos un repaso rápido:
* Capa de Presentación o interfaz de usuario (User Interface): donde los datos son presentados. Esto se refiere a un elemento avanzado de red como un firewall (cortafuegos) o un balanceador de carga (Load Balancer), con funciones de NAT
* Capa de Aplicación: donde los datos son procesados. Esto es un servidor web/aplicación, de cualquier tipo Apache, Tomcat, Nginx, IIS, WebSphere, etc.
* Capa de Datos: donde los datos son almacenados. Una base de datos tradicional con cualquier motor: mySQL, PostgreSQL, MS-SQL, Oracle, DB2, etc.

En principio, estas tres capas se pueden poner en un solo servicio de computación aunque se recomienda siempre dividirlos para reducir el impacto de ataque y mejorar el rendimiento. A modo de ejemplo, podríamos hacer un servicio o aplicación con las siguientes formas:
1) Un único dispositivo que tiene todo instalado: base de datos, servidor web, servicios de IP pública (firewall, routing, TSL/SSL, etc.)
2) Dos dispositivos que dividen la carga en dos formas diferentes:
* Un dispositivo que comparte la función de base de datos y de servidor web; mientras el segundo dispositivo toma la función de red
* Un dispositivo de base de datos (back-end) y otro que hace la parte de servidor web (front-end) además de la parte de red (presentación)
3) Tres dispositivos que dividen completamente las tareas:
* Back-End: base de datos (nunca tendra IP pública, siempre estará aislada)
* Front-End: servidor web (que tampoco tendrá IP pública)
* Presentación: balanceador de carga (Load Balancer) que tendrá IP pública y hará la parte de seguridad, junto con el NAT de puertos/protocolos y el control de certificados SSL

Para iniciar la instalación, lo primero será conectarnos a nuestra máquina virtual con Linux usando nuestro cliente SSH favorito. Cada dirección IP será diferente para cada asistente, pero las credenciales de inicio seran identicas y vienen preconfiguradas.

Necesitamos instalar todo el software necesario para crear nuestro servicio web. La ventaja de Linux es que usa un sistema de paquetes mejorado comparado a Windows. Entre estos, podemos mencionar:
1) Apache
2) mySQL Database
3) PHP Framework
3) Git Client
