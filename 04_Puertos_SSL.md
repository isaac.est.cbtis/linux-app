# PUERTOS y CERTIFICADOS SSL

Es hora de introducir nuevos elementos en nuestra infraestructura para entender mejor lo que vamos a encontrarnos en el mundo real. De forma general, la aplicación no suele hacer la gestión de IPs públicas, redirección de puertos o descarga de certificados SSL. Esta tarea se suele asignar siempre al dispositivo externo de red, que podría ser de varios tipos:
1) Balanceador de carga, el más común
2) Firewall o Cortafuegos, pues algunos dispositivos actualmente incluyen muchas opciones de la capa OSI, como los llamados NextGen Firewalls de Palo Alto, Fortinet, Check Point, Cisco, Juniper, Huawei, etc. que incluyen muchas características dentro del stack de seguridad, hasta el punto de manejar VPNs, Antivirus, Malware, Proxy, etc.
3) Redes de entrega de contenido o CDN (de sus siglas en inglés Content Delivery Network)

Esta práctica pues se divide en tres elementos principales, aunque dos de ellos son el mismo paso en entornos diferentes:
1) Añadir Certificados SSL gratuitamente en nuestro servidor
2) Redireccionar puertos
3) Añadir Certificados SSL globalmente por DNS

Como primera opción, hemos de poner el Certificado SSL en nuestra máquina local, lo que requerirá dos pasos:
* Añadir una entrada DNS en mexi.tech con nuestra IP pública. Por ejemplo, juliphp.mexi.tech en la IP 3.138.108.70 (cada uno su propia IP) - nos pedira otro registro TXT en el DNS durante la creación de SSL Cert
* Pedir un certificado gratuito en Let’s Encrypt / Certbot


## Let's Encrypt
Let’s Encrypt es una Autoridad de Certificado (CA del ingles Certificate Authority) que facilita el uso gratuito de certificados SSL/TLS certificates requeridos para que un servidor web se ejecute de forma segura (HTTPS), mejorando la confianza entre los usuarios y asegurando los datos.

Primero, hemos de instalar el módulo de SSL para nuestro servidor Apache -usamos Amazon Linux 2- (también hay un proceso parecido si usamos Nginx) y los binarios de certbot:
```bash
sudo yum update -y
sudo yum install -y mod_ssl
cd /tmp
wget -O epel.rpm –nv \
https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y ./epel.rpm
sudo yum install -y python2-certbot-apache.noarch
```

Estos pasos configurarán el archivo:
```conf
/etc/httpd/conf.d/ssl.conf
```

Hemos de crear un certificado SSL falso inicialmente, que no será parte del resultado final, pero es necesario para poder instalar el nuevo certificado en Apache:
```bash
sudo /etc/pki/tls/certs/make-dummy-cert localhost.crt
sudo service httpd restart
```

Si ahora navegamos a nuestro sitio con HTTPS, nos dará un error en el certificado por ser firmado internamente:

![https_1.png](../img/https_1.png)

Pero, esto también nos permitirá ejecutar el comando para crear un certificado SSL gratis:
```bash
sudo certbot -i apache -a manual --preferred-challenges dns -d newsite.k8s.builders --register-unsafely-without-email 
```

Durante la creación, nos pedirá crear una entrada DNS de tipo TXT (ojo! No es Alias CNAME ni IP tipo A) que hemos de crear en NameCheap antes de pulsar ‘Enter’:

![cert_1.png](../img/cert_1.png)


![cert_2.png](../img/cert_2.png)

El mismo asistente nos continuará dando pistas para configurar nuestro servidor. Le diremos que configure VHOST para nosotros pulsando 1 en el cambio de ssl.conf:

![cert_3.png](../img/cert_3.png)

Por último, nos pedirá si queremos redireccionar el puerto 80 al puerto 443. En condiciones normales, le diríamos que sí, pero en este caso no vamos a hacerlo porque después pondremos un Balanceador de Carga para hacer esto:

![cert_4.png](../img/cert_4.png)

Nos imprimirá al final el estado:

![cert_5.png](../img/cert_5.png)

Ahora nuestra conexión si usamos HTTPS estará cifrada, pero aún tendremos HTTP disponible. El icono del candado nos lo refleja:

![cert_6.png](../img/cert_6.png)

Podemos mirar las propiedades también:

![cert_7.png](../img/cert_7.png)

