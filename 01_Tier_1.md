# SERVICIO WEB - TIER 1

Empezaremos por el servidor web y luego añadiremos la base de datos y la parte de PHP. Para ejecutaremos los siguientes comandos al conectarnos a la sesión SSH:
```bash
sudo su -
sudo yum update -y
sudo yum install -y httpd mariadb-server
sudo systemctl start httpd
sudo systemctl enable httpd
sudo systemctl is-enabled httpd
sudo usermod -a -G apache ec2-user
sudo chown -R ec2-user:apache /var/www
```

Ahora mismo, ya tenemos un servidor web con la página de inicio por defecto. Solo tenemos que ir a nuestra IP pública desde nuestro navegador en nuestra máquina local, no en el servidor. Por favor, nótese que cada uno tendrá una IP pública diferente:

![apache_1.png](../img/apache_1.png)

Procedemos con el resto de elementos y crearemos una pequeña página para PHP:
```bash
cat /etc/system-release
sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
sudo yum install -y mariadb-server
sudo systemctl restart httpd
```

Haremos un archivo básico y lo comprobaremos en nuestro servidor:
```php
echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
```

Ya tenemos dos piezas, nos falta la base de datos, que deberemos inicializarla:
```bash
sudo systemctl start mariadb
sudo mysql_secure_installation
```

Nos va a hacer unas preguntas y vamos a responder los siguiente:
```bash
Enter current password for root (enter for none): → en blanco, solo enter
Set root password? [Y/n] → Y
New password: → 6V##m%uy8moNPzFiafyWb7Ly
Re-enter new password: → 6V##m%uy8moNPzFiafyWb7Ly
Password updated successfully!
Remove anonymous users? [Y/n] → Y
Disallow root login remotely? [Y/n] → Y
Remove test database and access to it? [Y/n] → Y
Reload privilege tables now? [Y/n] → Y
```

Reiniciaremos la base de datos y activaremos para que se arranque automáticamente:
```bash
sudo systemctl stop mariadb
sudo systemctl start mariadb
sudo systemctl enable mariadb
```

Instalamos GIT y comprobamos la versión:
```bash
sudo yum install -y git
git --version
```

Crear un directorio para nuestros repositorios llamado “REPOS”
```bash
mkdir /REPOS
cd /REPOS
```

![shell_1.png](../img/shell_1.png)

Clonar unos repositorios de Internet para jugar
```bash
git clone https://github.com/Juli-BCN/web-outrun.git
git clone https://github.com/Juli-BCN/webfluids.git
git clone https://github.com/Juli-BCN/Funny_Eyes.git
git clone https://github.com/Juli-BCN/pacman.git
git clone https://github.com/Juli-BCN/web-getitdone.git
git clone https://github.com/Juli-BCN/personal_profile_sample.git
```

Por defecto, Apache deja sus archivos para el servidor web en la ruta: `/var/www/html/`

Pongamos un nuevo directorio virtual con juegos:
```bash
mkdir /var/www/html/outrun
mkdir /var/www/html/pacman
mkdir /var/www/html/webfluids
mkdir /var/www/html/getitdone
mkdir /var/www/html/funnyeyes
```

Copiamos los archivos y los visualizamos en Pongamos un nuevo directorio virtual con juegos:
```bash
cp -R web-outrun/* /var/www/html/outrun
cp -R pacman/src/* /var/www/html/pacman
cp -R web-getitdone/* /var/www/html/getitdone
cp -R Funny_Eyes/* /var/www/html/funnyeyes
cp -R webfluids/* /var/www/html/webfluids
```

* Si Webfluids da error, ejecutar este comando:
```bash
mv /var/www/html/webfluids/index.htm /var/www/html/webfluids/index.html
```

Ahora podemos navegar a nuestra IP publica y añadir el directorio virtual que hemos creado, como por ejemplo: http://3.143.231.24/outrun/

![outrun_2.png](../img/outrun_2.png)

O Pacman: http://3.143.231.24/pacman/

![pacman_2.png](../img/pacman_2.png)

Webfluids: podemos hacerlo en el móvil para que sea más divertido:

![webfluids_2.png](../img/webfluids_2.png)

Ahora tenemos que instalar una aplicación con base de datos para completar la arquitectura a tres niveles. Preparamos el entorno:
```bash
git clone https://github.com/Juli-BCN/PHP_URL_Shortener.git
cp -R PHP_URL_Shortener/* /var/www/html
rm -rf /var/www/html/phpinfo.php
```

Nos toca conectarnos por línea de comando a nuestra base de datos, ejecutando:
```bash
mysql -h localhost -P 3306 -u root -p
→ 6V##m%uy8moNPzFiafyWb7Ly
```

![shell_2.png](../img/shell_2.png)

Una vez conectados, ejecutaremos los siguientes comandos:
```sql
CREATE USER 'lpurlsuser'@'localhost';
ALTER USER 'lpurlsuser'@'localhost' IDENTIFIED BY 'lpurlspasswd';
GRANT ALL PRIVILEGES ON *.* TO 'lpurlsuser'@'localhost';
FLUSH PRIVILEGES;
CREATE DATABASE LPURLS;
```

Nos desconectaremos con el comando:
```bash
exit;
```

Y nos conectaremos como nuestro usuario:
```bash
mysql -h localhost -P 3306 -u lpurlsuser -p
→ lpurlspasswd
```

Debemos crear una tabla llamada `testurls` con tres campos:
* id: int
* url: varchar(512)
* shortened: varchar(255)

El codigo es:
```sql
USE LPURLS;
CREATE TABLE testurls(
   id INT AUTO_INCREMENT,
   url VARCHAR(512),
   shortened VARCHAR(255),
   PRIMARY KEY ( id )
);
SHOW tables;
exit;
```

![shell_3.png](../img/shell_3.png)

El Back-End de base de datos está listo, así que hemos de cargar la aplicación en el servidor, para lo que deberemos instalar PHP y modificar algún archivo. El archivo a modificar es: shorten.php. Utilizaremos el editor que nos guste mas (vi, vim o nano). En las líneas 65 y 66, encontramos estos valores de IP:
```bash
nano /var/www/html/shorten.php
```

```php
echo "Shortened URL is <a href=\"http://52.17.207.99/decoder.php?decode=". $shorturl ."\">http://test.com/". $shorturl ."</a>";
//echo "Shortened URL is <a href=\"http://52.17.207.99/url/". $shorturl ."\">http://test.com/". $shorturl ."</a>";
```

Cambiaremos la IP en cuestión (52.17.207.99) por nuestra IP pública para este taller (3.143.231.24). Todas serán diferentes, cada uno debe usar la suya.

Cargaremos nuestra aplicación y deberíamos ver lo siguiente:

![web_1.png](../img/web_1.png)

Pondremos un valor en campo URL (omitiendo la parte de protocolo http://). En mi caso, voy a utilizar fcbarcelona.cat, pero cada uno puede usar la URL que prefiera. Si estáis sin ideas, podéis utilizar: es.wikipedia.org/wiki/RTFM

Cuando pulsemos el botón ‘Shorten’, nos generará un nuevo link que sería el que podemos usar para recortar URLs:

![web_2.png](../img/web_2.png)

Se puede comprobar la lista desde nuestro cliente mySQL, ejecutando el comando:
```bash
mysql -h localhost -P 3306 -u lpurlsuser -p
```

→ lpurlspasswd
```sql
USE LPURLS;
select * from testurls;
```

Ya tenemos un servidor monolítico que tiene las tres capas de arquitectura. Nos queda dividir los servicios para que usemos tres elementos:
* Balanceador de Carga o Load Balancer
* Front-End WebServer con Apache
* Back-End WebServer con mySQL (en otro servidor que no tendrá acceso a Internet)

Además, hemos de cargar un certificado SSL y redireccionar el puerto 80 and 443 por seguridad.
