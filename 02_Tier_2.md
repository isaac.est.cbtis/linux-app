# BALANCEADOR DE CARGA - TIER 2

Esta es la parte más fácil, pues normalmente los balanceadores de carga suelen ser usados bajo demanda y tienen poca configuración, pues se pone en la DMZ.

Para mostrarlo, hemos de editar nuestro archivo “index.php” y la cabecera de la página para añadir nuestro nombre. Por ejemplo, esta línea 17:
```bash
nano /var/www/html/index.php
```

```php
<h2>PHP URL shortener</h2>
```

La cambiaremos por:
```php
<h2>PHP URL shortener - NOMBRE</h2>
```

* Que cada uno utilice su nombre

Por último, hemos de proceder al proceso de registro de servicios, para lo que deberemos escoger una forma de verificación para que el servicio esté arriba, conocido como “healthcheck”.

