# BASES DE DATOS - TIER 3

En este caso, la base de datos suele estar ya corriendo en alguna plataforma desde la que se pueda compartir. En nuestro caso, vamos a usar el servicio gestionado de AWS que se llama RDS y que permite utilizar las siguientes bases de datos:
1) mySQL
2) PostgreSQL
3) Microsoft SQL
4) Oracle
5) Aurora (es un fork propio de Amazon que puede utilizar el motor de mySQL o PostgreSQL)

En nuestro caso, como ya estamos utilizando mySQL, para no alterar el código, vamos a seguir con lo mismo, pero esta vez será Aurora mySQL en el back-end.
